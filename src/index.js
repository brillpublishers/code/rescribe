import React from "react";
import ReactDOM from "react-dom";
import Rescribe from "./components/Rescribe";

import "./styles.css";

function App() {

  const annotations = [];

  return (
    <div>
      <Rescribe 
        manifest="https://wellcomelibrary.org/iiif/b18035723/manifest" 
        startCanvas="1"
        annotations={annotations}
        leading="0"
        language="en"
        defaultFontSize="50"
        defaultLetterSpacing="1"
        defaultAnnotationHeight="100"
        placeholderText="type here"
        onSave={json => console.log(json)}
         />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
