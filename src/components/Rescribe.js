import React, { Component } from "react";
import AnnotationEditor from "./AnnotationEditor";
import AddAnnotation from "./AddAnnotation";
import classNames from "classnames";
import { utils } from "./Utils";
import {
  Manifest,
  CanvasProvider,
  SingleTileSource,
  Viewport,
  OpenSeadragonViewport,
  OpenSeadragonViewer,
  CanvasRepresentation
} from "@canvas-panel/core";

// Not yet part of public API
import EditableAnnotation from "@canvas-panel/core/es/components/EditableAnnotation/EditableAnnotation";

class Rescribe extends Component {
  viewport = React.createRef();

  constructor(props) {
    super(props);

    this.state = {
      manifest: this.props.manifest,
      startCanvas: Number(this.props.startCanvas),
      annotations: this.props.annotations || [],
      leading: this.props.leading,
      currentAnnotation: null,
      language: this.props.language,
      defaultFontSize: this.props.defaultFontSize,
      defaultLetterSpacing: this.props.defaultLetterSpacing,
      defaultAnnotationHeight: Number(this.props.defaultAnnotationHeight),
      placeholderText: this.props.placeholderText
    };

    this.handleSave = this.handleSave.bind(this);
  }

  ratio = 0.5;

  getEditableAnnotationClassnames(annotation) {
    return classNames({
      "editable-annotation": true,
      active:
        this.state.currentAnnotation &&
        annotation.id === this.state.currentAnnotation.id
          ? true
          : false
    });
  }

  handleSave() {
    const json = JSON.stringify(this.state.annotations);
    this.props.onSave(json);
  }

  render() {
    return (
      <div style={{ overflow: "hidden" }}>
        <div
          style={{
            float: "left",
            background: "#000",
            position: "relative",
            width: "50%"
          }}
        >
          <Manifest url={this.state.manifest}>
            <CanvasProvider startCanvas={this.state.startCanvas}>
              <SingleTileSource>
                <Viewport
                  maxHeight={600}
                  setRef={v => {
                    this.viewport = v;
                  }}
                >
                  <OpenSeadragonViewport viewportController={true}>
                    <OpenSeadragonViewer maxHeight={1000} />
                  </OpenSeadragonViewport>
                  <CanvasRepresentation ratio={this.ratio}>
                    {this.state.annotations.map((annotation, idx) => (
                      <EditableAnnotation
                        key={idx}
                        {...annotation}
                        containerProps={{
                          className: this.getEditableAnnotationClassnames(
                            annotation
                          ),
                          onClick: ev => {
                            this.setState({
                              currentAnnotation: this.state.annotations.find(
                                anno => anno.id === annotation.id
                              )
                            });
                          }
                        }}
                        boxStyles={{
                          fontSize: `${annotation.body[0].value.fontSize}px`,
                          letterSpacing: `${annotation.body[0].value.letterSpacing}px`
                        }}
                        ratio={this.ratio}
                        setCoords={(annotationIdx => xywh => {
                          const newAnnotations = JSON.parse(
                            JSON.stringify(this.state.annotations)
                          );
                          newAnnotations[annotationIdx] = {
                            ...this.state.annotations[annotationIdx],
                            ...xywh
                          };
                          this.setState({
                            annotations: newAnnotations
                          });
                        })(idx)}
                      >
                        {annotation.body[1].value}
                      </EditableAnnotation>
                    ))}
                  </CanvasRepresentation>
                </Viewport>
              </SingleTileSource>
            </CanvasProvider>
          </Manifest>
        </div>
        <div style={{ float: "left", width: "50%" }}>
          <div style={{ padding: "20px" }}>
            <Manifest url={this.state.manifest}>
              <CanvasProvider startCanvas={this.state.startCanvas}>
                {canvasProviderProps => (
                  <div>
                    <div>
                      <AddAnnotation
                        addAnnotation={annotation => {
                          utils.updateAnnotationStyles(
                            annotation,
                            annotation.body[0].value.fontSize,
                            annotation.body[0].value.letterSpacing
                          );
                          this.setState({
                            annotations: [
                              ...this.state.annotations,
                              annotation
                            ],
                            currentAnnotation: annotation
                          });
                        }}
                        {...canvasProviderProps}
                        language={this.state.language}
                        leading={this.state.leading}
                        ratio={this.ratio}
                        annotations={this.state.annotations}
                        currentAnnotation={
                          this.state.currentAnnotation
                            ? this.state.currentAnnotation
                            : this.state.annotations[
                                this.state.annotations.length - 1
                              ]
                        }
                        defaultAnnotationHeight={
                          this.state.defaultAnnotationHeight
                        }
                        defaultFontSize={this.state.defaultFontSize}
                        defaultLetterSpacing={this.state.defaultLetterSpacing}
                      />
                    </div>
                    <div>
                      <input
                        type="range"
                        onChange={ev => {
                          ev.persist();
                          this.setState({
                            leading: ev.target.value
                          })
                        }}
                        id="leading"
                        name="leading"
                        min="0"
                        max="1"
                        step=".1"
                        defaultValue="0"
                      />
                      <label>Leading</label>
                    </div>
                    <div>
                      <AnnotationEditor
                        annotation={this.state.currentAnnotation}
                        fontSize={this.state.fontSize}
                        letterSpacing={this.state.letterSpacing}
                        placeholderText={this.state.placeholderText}
                        onDelete={annotation => {
                          const annotations = [...this.state.annotations]; // make a separate copy of the array
                          const index = annotations.findIndex(x => {
                            return x.id === annotation.id;
                          });
                          if (index !== -1) {
                            annotations.splice(index, 1);
                            this.setState({
                              currentAnnotation: null,
                              annotations: annotations
                            });
                          }
                        }}
                        onValueChanged={value => {
                          const annotation = utils.getNewAnnotationWithId(this.state.annotations, this.state.currentAnnotation.id);
                          annotation.body[1].value = value;
                          this.setState({
                            currentAnnotation: annotation
                          });
                        }}
                        onFontSizeChanged={fontSize => {
                          const annotation = utils.getNewAnnotationWithId(this.state.annotations, this.state.currentAnnotation.id);
                          annotation.body[0].value.fontSize = Number(fontSize);
                          utils.updateAnnotationStyles(
                            annotation,
                            fontSize,
                            annotation.body[0].value.letterSpacing
                          );
                          this.setState({
                            currentAnnotation: annotation
                          });
                        }}
                        onLetterSpacingChanged={letterSpacing => {
                          const annotation = utils.getNewAnnotationWithId(this.state.annotations, this.state.currentAnnotation.id);
                          annotation.body[0].value.letterSpacing = Number(letterSpacing);
                          utils.updateAnnotationStyles(
                            annotation,
                            annotation.body[0].value.fontSize,
                            letterSpacing
                          );
                          this.setState({
                            currentAnnotation: annotation
                          });
                        }}
                      />
                    </div>
                  </div>
                )}
              </CanvasProvider>
            </Manifest>
            <button onClick={this.handleSave}>Save</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Rescribe;
