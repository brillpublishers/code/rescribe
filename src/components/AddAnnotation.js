import React, { Component } from "react";
import { utils } from "./Utils";

export default class AddAnnotation extends Component {
  render() {
    const {
      canvas,
      addAnnotation,
      annotations,
      leading,
      currentAnnotation,
      defaultAnnotationHeight,
      ratio,
      manifest,
      language,
      defaultFontSize,
      defaultLetterSpacing
    } = this.props;

    const x = currentAnnotation ? currentAnnotation.x : 0;
    const y = currentAnnotation
      ? currentAnnotation.y +
        currentAnnotation.height +
        currentAnnotation.height * leading
      : 0;
    const w = currentAnnotation
      ? currentAnnotation.width
      : canvas.getWidth() * ratio;
    const h = currentAnnotation
      ? currentAnnotation.height
      : defaultAnnotationHeight;

    // get the annotation with the highest index
    let id = -1;

    for (let i = 0; i < annotations.length; i++) {
      const anno = annotations[i];
      const index = utils.getAnnotationIndex(anno);

      if (index > id) {
        id = index;
      }
    }

    id++;

    return (
      <button
        onClick={() => {
          addAnnotation({
            id: `${manifest.id}/anno/${id}`,
            type: "annotation",
            motivation: "supplementing",
            x: x,
            y: y,
            width: w,
            height: h,
            target: {
              id: `${canvas.id}#xywh=${x},${y},${w},${h}`,
              type: "Image",
              format: "image/jpeg",
              styleClass: `.anno-${id}`
            },
            body: [
              {
                id: `${manifest.id}/anno/${id}/body/0`,
                type: "DataSet",
                value: {
                  fontSize: defaultFontSize,
                  letterSpacing: defaultLetterSpacing
                },
                format: "application/json"
              },
              {
                id: `${manifest.id}/anno/${id}/body/1`,
                type: "TextualBody",
                value: "",
                format: "text/html",
                language: language
              }
            ]
          });
        }}
      >
        Add Annotation
      </button>
    );
  }
}
