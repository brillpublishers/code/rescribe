import React, { Component } from "react";

export default class AnnotationEditor extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      id: "",
      fontSize: 0, 
      letterSpacing: 0,
      value: ""
    };

    this.handleValueChange = this.handleValueChange.bind(this);
    this.handleFontSizeChange = this.handleFontSizeChange.bind(this);
    this.handleLetterSpacingChange = this.handleLetterSpacingChange.bind(this);
  }

  handleValueChange(event) {
    this.props.onValueChanged(event.target.value);
  }

  handleFontSizeChange(event) {
    this.props.onFontSizeChanged(event.target.value);
  }

  handleLetterSpacingChange(event) {
    this.props.onLetterSpacingChanged(event.target.value);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.annotation) {
      this.setState({
        id: newProps.annotation.id,
        fontSize: Number(newProps.annotation.body[0].value.fontSize),
        letterSpacing: Number(newProps.annotation.body[0].value.letterSpacing),
        value: newProps.annotation.body[1].value
      });
    }
  }

  render() {
    const {
      annotation,
      onDelete,
      placeholderText
    } = this.props;

    return annotation ? (
      <div>
        {/**
        <input
          type="text"
          readOnly
          value={this.state.id}
          style={{ width: "350px" }}
        />
        */}
        <input
          type="text"
          onChange={this.handleValueChange}
          value={this.state.value}
          style={{ width: "350px" }}
          placeholder={placeholderText}
        /> 
        <button onClick={() => onDelete(annotation)}>Delete</button>
        <div>
          <input
            type="range"
            onChange={this.handleFontSizeChange}
            value={this.state.fontSize}
            id="fontsize"
            name="fontsize"
            min="20"
            max="400"
          />
          <label>Font Size</label>
        </div>
        <div>
          <input
            type="range"
            onChange={this.handleLetterSpacingChange}
            value={this.state.letterSpacing}
            id="letterspacing"
            name="letterspacing"
            min="1"
            max="50"
          />
          <label>Letter Spacing</label>
        </div>
      </div>
    ) : (
      <span />
    );
  }
}
