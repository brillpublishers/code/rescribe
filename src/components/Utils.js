export const utils = {
  getAnnotationIndex: annotation => {
    return Number(annotation.id.substr(annotation.id.lastIndexOf("/") + 1));
  },

  getNewAnnotationWithId: (annotations, id) => {
    const newAnnotations = [...annotations]; // make a separate copy of the array
    const index = newAnnotations.findIndex(x => {
      return x.id === id;
    });
    return newAnnotations[index];
  },

  updateAnnotationStyles: (annotation, fontSize, letterSpacing) => {
    const index = utils.getAnnotationIndex(annotation);
    annotation.stylesheet = {
      type: "CssStylesheet",
      value: `.anno-${index} { font-size: ${fontSize}; letter-spacing: ${letterSpacing} }`
    };
  }
};
